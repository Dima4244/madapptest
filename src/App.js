import React from "react";
import MadAppForm from"./components/form";
import "./App.css";
import Title from "./components/title";
import TextBlock from "./components/text-block";
import laptop from "./misc/img/laptop.svg";

function App() {
  return (
    <div className="App">
        <div className='form-container' >
            <Title
                subTitle={(
                    <>
                        {"Let us know"}
                        <br/>
                        {"how we can help"}
                    </>
                )}
            >
                Get in touch
            </Title>
            <div className="container">
            <div className='form --title'>
                <MadAppForm/>
            </div>
            <TextBlock title="What's next" className="text-block" >
                <p> We’ll contact you within a few hours with our next steps. We normally schedule a call with our engineers to discuss your project in more detail. If you’d like to sign an NDA, please let us know. We’ll prepare it for you.</p>
                <p>Since we live on two different continents (Australia and Europe) we are available around the clock.</p>
            </TextBlock>
            </div>
        </div>
        <TextBlock title="Visit us" className="visit-us">
            <img className='laptop' src={laptop} alt="laptop icon" />
            <p> Our company management and business development team is located in Sydney, Australia. Our engineering team is located in Kremenchuk, Ukraine.</p>
        </TextBlock>
    </div>
  );
}

export default App;
