import React from"react";
import PropTypes from "prop-types";

import "./title.css";


const Title = ({children,subTitle})=>(
    <div className="title-container">
        <h1 className='title'>
            {children}
        </h1>
        <div className="subTitle" >{subTitle}</div>
    </div>
);

Title.propTypes = {
    children: PropTypes.string,
    subTitle: PropTypes.object
};

export  default Title;
