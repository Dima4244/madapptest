import React from"react";
import PropTypes from "prop-types";

import "./text-block.css";

const TextBlock = ({children,title,className})=>(
    <div className={className}>
        <h3 className='title'>
            {title}
        </h3>
        <div className="text" >{children}</div>
    </div>
);

TextBlock.propTypes = {
    children: PropTypes.array,
    title: PropTypes.string,
    className: PropTypes.string,
};

export  default TextBlock;
