import React, {useState} from "react";

import Input from "../../misc/input";
import Button from "../../misc/button";
import Textarea from "../../misc/textarea";

import './form.css'

const MadAppForm = ()=>{
    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [phone,setPhone] = useState("");
    const [message,setMessage] = useState("");
    const [status,setState] = useState({});

    const handleSubmit = (ev)=>{
        ev.preventDefault();
        const form = ev.target;
        const data = new FormData(form);
        const xhr = new XMLHttpRequest();
        xhr.open(form.method, form.action);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== XMLHttpRequest.DONE) return;
            if (xhr.status === 200) {
                form.reset();
                setState({ status: "SUCCESS" });
            } else {
                setState({ status: "ERROR" });
            }
        };
        xhr.send(data);
    };

    return(
        <form
            onSubmit={handleSubmit}
            action="https://formspree.io/xzbzwdpk"
            method="POST"
        >
            <Input
                type='text'
                name="Visitor name"
                className="simple-input"
                placeholder='Name'
                value={name}
                onChange={setName}
                required
            />
            <Input
                type='email'
                name='Visitor email'
                className="simple-input"
                placeholder='Email'
                value={email}
                onChange={setEmail}
                required
            />
            <Input
                type='text'
                name='Visitor phone'
                className="simple-input"
                placeholder='Phone number'
                value={phone}
                onChange={setPhone}
            />
            <Textarea
                name='Visitor message'
                className="simple-input"
                placeholder='Message'
                value={message}
                onChange={setMessage}
            />
            <Button className='button' text='Send'/>
        </form>
    );
};

export  default MadAppForm;
