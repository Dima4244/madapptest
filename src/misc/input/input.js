import React from "react";


import PropTypes from "prop-types";

const Input = ({type,className,placeholder,name, required,onChange})=>(
    <input
        type={type}
        name={name}
        className={className}
        placeholder={placeholder}
        required={required}
        onChange={onChange}
    />);

Input.propTypes = {
    type: PropTypes.string.isRequired,
    className:PropTypes.string.isRequired,
    name:PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    onChange: PropTypes.func
};

export  default  Input;
