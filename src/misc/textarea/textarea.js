import React from "react";


import PropTypes from "prop-types";

const Textarea = ({className,placeholder,name, required,onChange})=>(
    <textarea
        name={name}
        className={className}
        placeholder={placeholder}
        required={required}
        onChange={onChange}
    />);

Textarea.propTypes = {
    className:PropTypes.string.isRequired,
    name:PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    onChange: PropTypes.func
};

export  default  Textarea;
