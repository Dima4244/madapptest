import React from "react";
import PropTypes from "prop-types";


const Button = ({className,text})=> <button className={className}>{text}</button>;

Button.propTypes = {
    className:PropTypes.string.isRequired,
    text:PropTypes.string.isRequired
};

export default Button;
